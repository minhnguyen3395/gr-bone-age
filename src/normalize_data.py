import os
import numpy as np
from skimage.io import imsave, imread, imshow
from skimage.transform import resize
from skimage.exposure import equalize_adapthist, is_low_contrast
from matplotlib import pyplot as plt
from constants import boneage_test_path_maskonly, boneage_test_mask_path, constrast, unet_test_path, boneage_test_path, normalize_error_log_path, full_data_path, image_size_json_path, kp_full_json_path, unet_train_path, unet_mask_path, normalized_image_path, key_points_max_size, key_points_desired_size, desired_size
import cv2
import json
from math import pow, acos, sqrt, sin, cos
from os.path import join, isfile
from data_unet import gamma_correction	

debug = False
enhance_contrast = True
new_middle_tip_p = (800.0, 100.0)
new_center_capitate_p = (800.0, 1600.0)
new_capitate_middle = (0.0, -1500.0)

def preprocessing_image(image):
	img = np.zeros(key_points_max_size, dtype=np.float32)
	img[:image.shape[0], :image.shape[1]] = image
	img = cv2.resize(img, (key_points_desired_size[1], key_points_desired_size[0]), interpolation=cv2.INTER_LINEAR)
	return img

def normalized(img, pts):
	# The mission is finding new thumb position
	org_capitate_middle = pts[0] - pts[2]
	org_capitate_thumb = pts[1] - pts[2]
	org_capitate_middle = pts[0] - pts[2]

	org_mag_capitate_middle = magnitude_vec(org_capitate_middle)
	org_mag_capitate_thumb = magnitude_vec(org_capitate_thumb)
	new_mag_capitate_middle = magnitude_vec(new_capitate_middle)

	angle = get_angle(org_capitate_middle, org_capitate_thumb)

	org_dist_to_pivot = org_mag_capitate_thumb * cos(angle)
	org_dist_to_thumb = org_mag_capitate_thumb * sin(angle)

	ratio = new_mag_capitate_middle / org_mag_capitate_middle
	new_dist_to_pivot = ratio * org_dist_to_pivot
	new_dis_to_thumb = ratio * org_dist_to_thumb

	new_pivot_p = [new_center_capitate_p[0], new_center_capitate_p[1]]
	new_pivot_p[1] -= new_dist_to_pivot

	new_thumb_p = [new_pivot_p[0], new_pivot_p[1]]
	new_thumb_p[0] += new_dis_to_thumb

	new_pts = np.float32([new_middle_tip_p, new_thumb_p, new_center_capitate_p])
	M = cv2.getAffineTransform(pts,new_pts)
	img = cv2.warpAffine(img,M,(key_points_desired_size[1], key_points_desired_size[0]))
	return img

def magnitude_vec(p1, p2):
	return sqrt(pow(p2[0] - p1[0], 2) + pow(p2[1] - p1[1], 2))

def magnitude_vec(p):
	return sqrt(pow(p[0], 2) + pow(p[1], 2))

def get_angle(p1, p2):
	dot_product = p1[0] * p2[0] + p1[1] * p2[1]
	cos = dot_product / magnitude_vec(p1) / magnitude_vec(p2)
	return acos(cos)

if __name__ == '__main__':
	# load image names
	image_names = os.listdir(unet_train_path)

	normalized_images = os.listdir(normalized_image_path)
	for normalized_image in normalized_images:
		image_names.remove(normalized_image)

	image_names.sort()

	# load json image sizes
	image_sizes = {}
	with open(image_size_json_path, 'r') as file:
		image_sizes = json.load(file)

	kp_data = {}
	with open(kp_full_json_path, 'r') as file:
		kp_data = json.load(file)

	if isfile(normalize_error_log_path):
		os.remove(normalize_error_log_path)
	size_error_log = []

	# crop image using mask
	for image_name in image_names:
		print(image_name)

		# check if image kp exists
		if image_name in kp_data:
			# create masked image
			image = cv2.imread(join(unet_train_path, image_name), 0)

			mask_path = join(unet_mask_path, image_name)

			if isfile(mask_path):
				mask = cv2.imread(mask_path, 0)

				if image.shape == mask.shape:
					image = cv2.bitwise_and(image, image, mask=mask)

					if enhance_contrast and is_low_contrast(image, 0.8):
						image = gamma_correction(image, 1.1)
						print('Low contrast')
						image = equalize_adapthist(image, clip_limit=constrast)
						image = preprocessing_image(image)
					else:
						image = preprocessing_image(image)
						image /= 255.0

					# save mask only preprocessing image
					# imsave(join(boneage_test_path_maskonly, image_name), image)

					# affine transformation
					kp = kp_data[image_name]

					middle_finger_p = (kp[0], kp[1])
					thumb_p = (kp[2], kp[3])
					center_capitate_p = (kp[4], kp[5])
					img = normalized(image, np.float32([middle_finger_p, thumb_p, center_capitate_p]))

					if debug:
						imshow(image)
						plt.show()

					imsave(join(normalized_image_path, image_name), img)
				else:
					size_error_log.append(image_name)
					print('Image size is different from mask size')
			else:
				if enhance_contrast and is_low_contrast(image, 0.5):
					image = gamma_correction(image, 1.5)
					print('Low contrast')
					image = equalize_adapthist(image, clip_limit=constrast)
					image = preprocessing_image(image)
				else:
					image = preprocessing_image(image)
					image /= 255.0					

				if debug:
					imshow(image)
					plt.show()

				# affine transformation
				kp = kp_data[image_name]

				middle_finger_p = (kp[0], kp[1])
				thumb_p = (kp[2], kp[3])
				center_capitate_p = (kp[4], kp[5])
				img = normalized(image, np.float32([middle_finger_p, thumb_p, center_capitate_p]))
				imsave(join(boneage_test_path, image_name), img)

		else:
			print('Image does not have kp data')

	f = open(normalize_error_log_path, 'w')
	for error in size_error_log:
		f.write(error)
	f.close()