from constants import model_gender_path
from train_gender import get_gender_model, batch_size
from train_boneage import preprocess
from data_gender import load_test
import numpy as np

imgs_test, img_test_id, labels_test = load_test()
imgs_test = preprocess(imgs_test)
total = len(imgs_test)

model_gender = get_gender_model()
model_gender.load_weights(model_gender_path)

gender_prob = model_gender.predict(imgs_test, verbose=1, batch_size=batch_size)
predicts = np.ndarray((total,), dtype=np.uint8)

for i in xrange(total):
	predict = np.argmax(gender_prob[i])
	predicts[i] = predict

np.save('boneage_gender_predict.npy', predicts)

correct = 0
i = 0
for label in labels_test:
	if label == predicts[i]:
		correct += 1
	else:
		print(img_test_id[i])
		print('Predict: ' + str(predicts[i]))
		print('Label: ' + str(label))
	i += 1

acc = 1.0 * correct / total
print('Accuracy: ' + str(acc))