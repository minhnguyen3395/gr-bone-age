import json
import os
import numpy as np
from skimage.io import imread
from constants import full_data_path

analyze = False
plot = True

if __name__ == '__main__':
	if analyze:
		with open('image_size.json', 'w') as output:
				data = {}
				full_names = os.listdir(full_data_path)

				total = len(full_names)
				i = 0
				for name in full_names:
					i += 1
					image = imread(os.path.join(full_data_path, name))
					data[name] = image.shape
					print(str(i) + '/' + str(total))

				json.dump(data, output)

	if plot:
		with open('image_size.json', 'r') as file:
			data = {}
			data = json.load(file)
			image_names = data.keys()
			large_image_count = 0

			# plot size distribution
			size_distribution = {}

			for name in image_names:
				if data[name][0] > 2570 or data[name][1] > 2570:
					large_image_count += 1

				key = str(data[name])
				if key in size_distribution:
					size_distribution[key] += 1
				else:
					size_distribution[key] = 1

			sizes = size_distribution.keys()
			counts = size_distribution.values()
			print(len(sizes))
			print(large_image_count)
				