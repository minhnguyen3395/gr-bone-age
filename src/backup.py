# Using to backup data structure in project in order to fast move between computer

import os
from os.path import join, exists
import shutil
import sys
import json
from constants import kp_train_path, kp_test_path, unet_train_path, unet_test_path, unet_mask_path, full_data_path

def backup():
	data = {}

	kp_train_data = os.listdir(kp_train_path)
	kp_test_data = os.listdir(kp_test_path)
	unet_train_data = os.listdir(unet_train_path)
	unet_test_data = os.listdir(unet_test_path)
	unet_mask_data = os.listdir(unet_mask_path)

	kp_train_data.sort()
	kp_test_data.sort()
	unet_train_data.sort()
	unet_test_data.sort()
	unet_mask_data.sort()

	data['kp_train'] = kp_train_data
	data['kp_test'] = kp_test_data
	data['unet_train'] = unet_train_data
	data['unet_test'] = unet_test_data
	data['unet_mask'] = unet_mask_data

	with open('backup.json', 'w') as f:
		json.dump(data, f)

def restore():
	data = {}

	with open('backup.json', 'r') as f:
		data = json.load(f)

	kp_train_data = data['kp_train']
	kp_test_data = data['kp_test']
	unet_train_data = data['unet_train']
	unet_test_data = data['unet_test']
	unet_mask_data = data['unet_mask']

	for data in kp_train_data:
		shutil.copy(join(full_data_path, data), join(kp_train_path, data))
		print('kp train ' + data)

	for data in kp_test_data:
		shutil.copy(join(full_data_path, data), join(kp_test_path, data))
		print('kp test ' + data)

	for data in unet_train_data:
		if 'aug' not in data:
			shutil.copy(join(full_data_path, data), join(unet_train_path, data))
			print('unet train ' + data)

	for data in unet_test_data:
		shutil.copy(join(full_data_path, data), join(unet_test_path, data))
		print('unet test ' + data)

	for data in unet_mask_data:
		if 'aug' not in data:
			shutil.copy(join(full_data_path, data), join(unet_mask_path, data))
			print('unet mask ' + data)

if __name__ == '__main__':
	option = sys.argv[1]

	if option == 'b':
		backup()
	elif option == 'r':
		restore()
	else:
		print('option b for backup and r for restore')