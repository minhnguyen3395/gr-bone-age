from constants import model_gender_path, model_full_reg_path, model_male_reg_path, model_female_reg_path, boneage_csv_path
from train_boneage import get_boneage_model, get_male_model, batch_size, preprocess, get_female_model
from data_boneage import load_test, load_test_maskonly, load_test_raw
import numpy as np
from train_gender import get_gender_model
from pandas import DataFrame, read_csv
import pandas as pd
	
use_gender_in_csv = False

def get_mae(predict, label, ids):
	total = len(predict)
	error_sum = 0
	for i in xrange(total):
		img_id = ids[i]
		pred_age = predict[i]
		label_age = label[i]
		error_sum += abs(pred_age - label_age)
		# print('-'*30)
		# print('Image ' + str(img_id))
		# print('Predict: ' + str(pred_age))
		# print('Label: ' + str(label_age))

	mae = 1.0 * error_sum / total
	print('MAE: ' + str(round(mae, 2)))
	print('-'*30)

def predict(imgs_test, imgs_test_id, total):
	if use_gender_in_csv:
		boneage_csv = pd.read_csv(boneage_csv_path)
		boneage_csv.set_index('id', inplace=True)

	# gender classification
	model_gender = get_gender_model()
	model_gender.load_weights(model_gender_path)

	# full reg
	model_full_reg = get_boneage_model()
	model_full_reg.load_weights(model_full_reg_path)

	gender_pred = model_gender.predict(imgs_test, verbose=1, batch_size=batch_size)
	full_reg_pred = model_full_reg.predict(imgs_test, verbose=1, batch_size=batch_size)

	# male reg
	model_male_reg = get_male_model()
	model_male_reg.load_weights(model_male_reg_path)

	# female reg
	model_female_reg = get_female_model()
	model_female_reg.load_weights(model_female_reg_path)

	# predict
	male_reg_pred = model_male_reg.predict(imgs_test, verbose=1, batch_size=batch_size)
	female_reg_pred = model_female_reg.predict(imgs_test, verbose=1, batch_size=batch_size)

	# ensemble
	mixed_results = np.ndarray((total,), dtype=np.float32)
	male_female_results = np.ndarray((total,), dtype=np.float32)
	ensemble_results = np.ndarray((total,), dtype=np.int32)

	for i in xrange(total):
		full_reg_prob = full_reg_pred[i][0]
		male_reg_prob = male_reg_pred[i][0]
		female_reg_prob = female_reg_pred[i][0]

		predict_class = 0
		gender_class = 0

		if use_gender_in_csv:
			gender_class = boneage_csv.loc[imgs_test_id[i], 'male']
		else:
			gender_class = np.argmax(gender_pred[i])

		# mixed only
		mixed_results[i] = full_reg_prob

		# male female only
		is_male = True if gender_class == 1 else False 
		if is_male:
			predict_class = male_reg_prob
		else:
			predict_class = female_reg_prob
		male_female_results[i] = predict_class

		# ensemble
		confident_diff = abs(gender_pred[i][0] - gender_pred[i][1])

		if confident_diff <= 0.1:
			predict_class = full_reg_prob
		else:
			is_male = True if gender_class == 1 else False 
			if is_male:
				predict_class = male_reg_prob
			else:
				predict_class = female_reg_prob
					
		ensemble_results[i] = predict_class

	# calculate MAE
	print("Mixed gender only")
	get_mae(mixed_results, labels_test, imgs_test_id)

	print('Male and female ensemble')
	get_mae(male_female_results, labels_test, imgs_test_id)

	print('All ensemble')
	get_mae(ensemble_results, labels_test, imgs_test_id)

if __name__ == '__main__':
	imgs_test, imgs_test_id, labels_test = load_test()
	imgs_test_maskonly = load_test_maskonly()
	imgs_test_raw = load_test_raw()

	imgs_test = preprocess(imgs_test)
	total = len(imgs_test)
	imgs_test_maskonly = preprocess(imgs_test_maskonly)
	imgs_test_raw = preprocess(imgs_test_raw)

	print('='*60)
	print('USING GENDER FROM GENDER MODEL RESULT')
	print('Predict on non-preprocessing data')
	predict(imgs_test_raw, imgs_test_id, total)

	print('Predict on segmentation only data')
	predict(imgs_test_maskonly, imgs_test_id, total)

	print('Predict on full preprocessing data')
	predict(imgs_test, imgs_test_id, total)

	use_gender_in_csv = True

	print('='*60)
	print('USING GENDER FROM CSV')
	print('Predict on non-preprocessing data')
	predict(imgs_test_raw, imgs_test_id, total)

	print('Predict on segmentation only data')
	predict(imgs_test_maskonly, imgs_test_id, total)

	print('Predict on full preprocessing data')
	predict(imgs_test, imgs_test_id, total)