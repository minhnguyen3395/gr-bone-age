import numpy as np
from keras.models import Model
from data_unet import load_test_data, desired_size, gamma_correction
from train_unet import preprocess, batch_size
import os
from skimage.io import imsave, imshow
from constants import mask_raw_path, get_unet, trained_unet_model_path, constrast
import sys
from skimage.exposure import equalize_adapthist, is_low_contrast
from matplotlib import pyplot as plt

if len(sys.argv) == 1:
	print('Argument number error. Using p for prediction, l for loading image from npy')
	quit()
	
option = sys.argv[1]
imgs_mask_test = None

print('-'*30)
print('Loading and preprocessing test data...')
print('-'*30)

plt.show()
if option == 'p':
	imgs_test, imgs_id_test = load_test_data()
	imgs_test = preprocess(imgs_test)

	print('-'*30)
	print('Loading saved weights...')
	print('-'*30)
	model = get_unet()
	model.load_weights(trained_unet_model_path)

	print('-'*30)
	print('Predicting masks on test data...')
	print('-'*30)
	imgs_mask_test = model.predict(imgs_test, verbose=1, batch_size=batch_size)
	np.save('imgs_mask_test.npy', imgs_mask_test)

	print('-' * 30)
	print('Saving predicted masks to files...')
	print('-' * 30)
	pass
elif option == 'l':
	imgs_mask_test = np.load('imgs_mask_test.npy')


if not os.path.exists(mask_raw_path):
    os.mkdir(mask_raw_path)

mask_size = (desired_size, desired_size)
for image, image_id in zip(imgs_mask_test, imgs_id_test):
	image = (image[:, :, 0])
	print(image_id)
	imsave(os.path.join(mask_raw_path, str(image_id) + '.png'), image)
