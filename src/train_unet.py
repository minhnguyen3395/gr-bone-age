from __future__ import print_function

from skimage.transform import resize
from skimage.io import imsave, imshow
import numpy as np
from keras.models import Model
from keras.layers import Dense, Dropout, Flatten, Conv2D, MaxPooling2D, BatchNormalization, Input, merge, UpSampling2D, Cropping2D, ZeroPadding2D, Reshape, core, Convolution2D, Activation
from keras.callbacks import ModelCheckpoint, EarlyStopping, ReduceLROnPlateau
from keras import backend as K
from keras.layers.merge import concatenate
import cv2
import matplotlib.pyplot as plt
from data_unet import load_train_data
from constants import img_cols, img_rows, get_unet, trained_unet_model_path

K.set_image_data_format('channels_last')  # TF dimension ordering in this code

batch_size = 12
epochs = 300
validation_split = 0.1

def preprocess(imgs):
    for i in range(imgs.shape[0]):
        imgs[i] = resize(imgs[i], (img_rows, img_cols), preserve_range=True)
    imgs = imgs[..., np.newaxis]
    return imgs

def train():
    print('-'*30)
    print('Loading and preprocessing train data...')
    print('-'*30)

    X_train, Y_train = load_train_data()

    # imshow(X_train[0])
    # plt.show()
    # imshow(Y_train[0])
    # plt.show()

    X_train = preprocess(X_train)
    Y_train = preprocess(Y_train)

    print('-'*30)
    print('Creating and compiling model...')
    print('-'*30)
    model = get_unet()
    # model.load_weights(trained_unet_model_path)
    model.summary()
    early_stopping = EarlyStopping(patience=10, verbose=1)
    model_checkpoint = ModelCheckpoint(trained_unet_model_path, monitor='val_loss', save_best_only=True)
    reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=5, 
        verbose=1, mode='auto', epsilon=0.0001, cooldown=1, min_lr=1e-6)

    print('-'*30)
    print('Fitting model...')
    print('-'*30)

    history = model.fit(X_train, Y_train, batch_size=batch_size, epochs=epochs, verbose=1, shuffle=True,
             validation_split=validation_split,
             callbacks=[model_checkpoint, early_stopping, reduce_lr])

    print(history.history.keys())

    plt.plot(history.history['dice_coeff'])
    plt.plot(history.history['val_dice_coeff'], linestyle=':')
    plt.title('Dice Coefficient')
    plt.ylabel('dice coeff')
    plt.xlabel('epoch')
    plt.legend(['train', 'validation'], loc='upper left')
    plt.savefig('unet_dicecoeff.png', bbox_inches='tight')
    # plt.show()

    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'], linestyle=':')
    plt.title('Loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'validation'], loc='upper left')
    plt.savefig('unet_loss.png', bbox_inches='tight')
    # plt.show()

if __name__ == '__main__':
    train()