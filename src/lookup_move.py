import os
import shutil
import sys
from os.path import exists, join

lookup_dir = sys.argv[1]
src_dir = sys.argv[2]
dest_dir = sys.argv[3]

lookup_names = os.listdir(lookup_dir)
lookup_names.sort()

for name in lookup_names:
	src_path = join(src_dir, name)
	if exists(src_path):
		shutil.move(src_path, join(dest_dir, name))

