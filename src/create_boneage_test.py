import os
from constants import normalized_image_path, boneage_test_path, boneage_csv_path
import pandas as pd
import shutil
from os.path import join
import random

test_sample_number = 200

train_images = os.listdir(normalized_image_path)

boneage_csv = pd.read_csv(boneage_csv_path)
boneage_csv.set_index('id', inplace=True)

male_ids = []
female_ids = []

sample_per_class = test_sample_number / 2

for image_name in train_images:
	img_id = int(image_name.replace('.png', ''))
	is_male = boneage_csv.loc[img_id, 'male']

	if is_male:
		male_ids.append(image_name)
	else:
		female_ids.append(image_name)

male_ids = random.sample(male_ids, sample_per_class)
female_ids = random.sample(female_ids, sample_per_class)

for male_id in male_ids:
	shutil.move(join(normalized_image_path, male_id), join(boneage_test_path, male_id))

for female_id in female_ids:
	shutil.move(join(normalized_image_path, female_id), join(boneage_test_path, female_id))