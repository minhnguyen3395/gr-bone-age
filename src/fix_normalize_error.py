import os
from os.path import join, exists
from constants import full_data_path, trained_unet_model_path, normalize_error_log_path, normalized_image_path, normalize_fix_path, unet_mask_path,key_points_max_size
import shutil
import numpy as np
from skimage.io import imread, imshow
from skimage.transform import resize
from matplotlib import pyplot as plt
from train_unet import preprocess, batch_size
import sys
import cv2

if len(sys.argv) > 1 and sys.argv[1] == 'data':
	f = open(normalize_error_log_path, 'r')
	error_data_raw = f.read()

	error_ids = error_data_raw.split('.png')
	error_ids = error_ids[:-1] # remove the last element that's not a file name
	total = len(error_ids)

	for i in xrange(total):
		error_ids[i] += '.png'

	# imgs_fix = np.ndarray((total, key_points_max_size[0], key_points_max_size[1]), dtype=np.float32)

	# i = 0
	for image_id in error_ids:
		shutil.copy(join(full_data_path, image_id), join(normalize_fix_path, image_id))
		# img = imread(join(full_data_path, image_id), as_grey=True)
		# container = np.zeros(key_points_max_size, dtype=np.float32)
		# container[:img.shape[0], :img.shape[1]] = img
		# imgs_fix[i] = container
		# i += 1

	# np.save('fix_image.npy', imgs_fix)

# test_imgs = np.load('fix_image.npy')
# test_imgs = preprocess(test_imgs)

# print('-'*30)
# print('Loading saved weights...')
# print('-'*30)
# model = get_unet()
# model.load_weights(trained_unet_model_path)

# print('-'*30)
# print('Predicting masks on test data...')
# print('-'*30)
# imgs_mask_test = model.predict(imgs_test, verbose=1, batch_size=batch_size)

# print('-' * 30)
# print('Saving predicted masks to files...')
# print('-' * 30)

# if not os.path.exists(normalize_fix_path):
#     os.mkdir(normalize_fix_path)

# with open('image_size.json') as data_file:
# 		image_size_data = json.load(data_file)

# 		index = 0
# 		for mask_name in error_ids:
# 			print(mask_name)

# 			# Get size of original train image
# 			image_size = image_size_data[mask_name]
# 			h = image_size[0]
# 			w = image_size[1]
# 			img_mask = imgs_mask_test[index]

# 			# Only take portion of original train image
# 			img_mask = img_mask[:h, :w]

# 			_,img_mask = cv2.threshold(img_mask,100,255,cv2.THRESH_BINARY)

# 			# Find contours
# 			im2, contours, hierarchy = cv2.findContours(img_mask,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)

# 			# Find the largest contour
# 			largest_index = 0
# 			largest_area = 0
# 			i = -1
# 			for contour in contours:
# 				i += 1
# 				area = cv2.contourArea(contour)
# 				if area > largest_area:
# 					largest_area = area
# 					largest_index = i

# 			hand_contour = contours[largest_index]
#  			contour_mask = np.ones(img_mask.shape[:2], dtype="uint8") * 255

#  			# Draw removed contour on mask
# 			i = -1
# 			for contour in contours:
# 				i += 1
# 				if i != largest_index:
# 					cv2.drawContours(contour_mask, [contour], -1, 0, -1)

# 			# Using mask to remove undesirable contour
# 			img_mask = cv2.bitwise_and(img_mask, img_mask, mask=contour_mask)

# 			img_mask = cv2.drawContours(img_mask, [hand_contour], -1, (255 ,255, 255), thickness=cv2.FILLED)
# 			imsave(os.path.join(normalize_fix_path, mask_name), img_mask)
# 			index += 1