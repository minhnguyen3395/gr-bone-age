import os
import shutil
from constants import boneage_test_path, normalized_image_path

test_images = os.listdir(boneage_test_path)

for test_image in test_images:
	os.remove(os.path.join(normalized_image_path, test_image))