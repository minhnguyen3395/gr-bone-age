from pandas import DataFrame, read_csv
import pandas as pd
import os
from os.path import join
from constants import boneage_test_path, boneage_csv_path, normalized_image_path, boneage_val_split, boneage_train_col, boneage_train_row
import numpy as np
from sklearn.model_selection import train_test_split
from skimage.io import imsave, imread, imshow
from skimage.transform import resize
from matplotlib import pyplot as plt
from keras.utils import to_categorical
from data_unet import make_square_image
from data_boneage import preprocess

def create_train(boneage_csv):
	print('Starting creating training data')
	# load images
	image_names = os.listdir(normalized_image_path)
	image_names.sort()
	total = len(image_names)

	imgs = np.ndarray((total, boneage_train_row, boneage_train_col), dtype=np.float32)
	labels_gender = np.ndarray((total,), dtype=np.uint8)

	i = 0
	for image_name in image_names:
		print(image_name)
		img_id = int(image_name.replace('.png', ''))

		img = imread(join(normalized_image_path, image_name), as_grey=True)
		img = preprocess(img)
		# imshow(img)
		# plt.show()
		img = np.array([img], dtype=np.float32)
		imgs[i] = img

		is_male = boneage_csv.loc[img_id, 'male']
		labels_gender[i] = int(is_male)
		i += 1

	imgs, imgs_val, labels_gender, labels_gender_val = train_test_split(
		imgs, labels_gender, test_size=boneage_val_split, stratify=labels_gender, random_state=33)

	labels_gender = to_categorical(labels_gender, num_classes=2)
	labels_gender_val = to_categorical(labels_gender_val, num_classes=2)
	
	print(imgs.shape)
	print(imgs_val.shape)
	print(labels_gender.shape)
	print(labels_gender_val.shape)

	np.save('boneage_img_gender.npy', imgs)
	np.save('boneage_label_gender.npy', labels_gender)
	np.save('boneage_img_gender_val.npy', imgs_val)
	np.save('boneage_label_gender_val.npy', labels_gender_val)

def create_test(boneage_csv):
	print('Starting creating testing data')
	image_names = os.listdir(boneage_test_path)
	image_names.sort()
	total = len(image_names)
	imgs_test = np.ndarray((total, boneage_train_row, boneage_train_col), dtype=np.float32)
	imgs_test_id = np.ndarray((total,), dtype=np.int32)
	labels_test = np.ndarray((total,), dtype=np.uint8)

	i = 0
	for image_name in image_names:
		print(image_name)
		img_id = int(image_name.split('.')[0])

		img = imread(join(boneage_test_path, image_name), as_grey=True)
		img = preprocess(img)
		img = np.array([img], dtype=np.float32)
		imgs_test[i] = img
		imgs_test_id[i] = img_id

		is_male = boneage_csv.loc[img_id, 'male']
		labels_test[i] = int(is_male)
		i += 1

	np.save('boneage_test_img_gender.npy', imgs_test)
	np.save('boneage_test_id_gender.npy', imgs_test_id)
	np.save('boneage_test_label_gender.npy', labels_test)

def load_train():
	imgs = np.load('boneage_img_gender.npy')
	labels_gender = np.load('boneage_label_gender.npy')
	return imgs, labels_gender

def load_validation():
	imgs_val = np.load('boneage_img_gender_val.npy')
	labels_val = np.load('boneage_label_gender_val.npy')
	return imgs_val, labels_val

def load_test():
	imgs_test = np.load('boneage_test_img_gender.npy')
	imgs_test_id = np.load('boneage_test_id_gender.npy')
	labels_test = np.load('boneage_test_label_gender.npy')
	return imgs_test, imgs_test_id, labels_test

def analyze_gender(boneage_csv):
	# Number of radiographs per gender 
	genders = boneage_csv.loc[:, 'male'].tolist()
	count = [0, 0]
	for is_male in genders:
		if is_male:
			count[0] += 1
		else:
			count[1] += 1

	objects = ('Male', 'Female')
	y_pos = np.arange(len(objects))

	plt.bar(y_pos, count, align='center', alpha=0.5)
	plt.xticks(y_pos, objects)
	plt.ylabel('Number of radiographs')
	plt.xlabel('Genders')
	plt.title('Gender Distribution')
	plt.show()

	# Number of radiographs per age in years
	ages_male = {}
	ages_female = {}
	ages = boneage_csv.loc[:, 'boneage'].tolist()
	ages[:] = [x / 12 for x in ages]

	i = 0
	for age in ages:
		is_male = genders[i]

		if is_male:
			if age in ages_male:
				ages_male[age] += 1
			else:
				ages_male[age] = 1
		else:
			if age in ages_female:
				ages_female[age] += 1
			else:
				ages_female[age] = 1
		i += 1
	print(ages_male)
	print(ages_female)
	# Female plot
	age_values = ages_female.keys()
	y_pos = np.arange(len(age_values))
	plt.bar(y_pos, ages_female.values(), align='center', alpha=0.5)
	plt.xticks(y_pos, age_values)
	plt.ylabel('Number of radiographs')
	plt.xlabel('Bone age in years')
	plt.title('Females')
	plt.show()

	# Male plot
	age_values = ages_male.keys()
	y_pos = np.arange(len(age_values))
	plt.bar(y_pos, ages_male.values(), align='center', alpha=0.5)
	plt.xticks(y_pos, age_values)
	plt.ylabel('Number of radiographs')
	plt.xlabel('Bone age in years')
	plt.title('Males')
	plt.show()

	
if __name__ == '__main__':
	boneage_csv = pd.read_csv(boneage_csv_path)
	boneage_csv.set_index('id', inplace=True)

	# analyze_gender(boneage_csv)
	create_train(boneage_csv)
	create_test(boneage_csv)
