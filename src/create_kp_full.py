import os
from constants import kp_json_path, kp_full_json_path, kp_result_path, kp_test_path
import json
import numpy as np
import math

test_images = os.listdir(kp_test_path)
test_images.sort()
test_kps = np.load(kp_result_path)

kp_data = {}
with open(kp_json_path, 'r') as file:
	kp_data = json.load(file)

i = 0
for test_image in test_images:
	print(test_image)
	test_kp = test_kps[i]
	kp_data[test_image] = []
	
	for coor in test_kp:
		coor = math.floor(coor)
		kp_data[test_image].append(coor)

	i += 1

with open(kp_full_json_path, 'w') as file:
	json.dump(kp_data, file)