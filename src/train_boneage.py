from keras.models import Model
from keras.layers import Multiply, Input, Conv2D, Dense, Dropout, MaxPooling2D, Activation, BatchNormalization, Flatten
from keras.callbacks import ModelCheckpoint, EarlyStopping, ReduceLROnPlateau
import numpy as np
from keras import backend as K
from skimage.io import imshow
from skimage.transform import resize, rotate
import matplotlib.pyplot as plt
from keras.optimizers import Adam, SGD
from constants import model_full_reg_path, model_full_class_path, boneage_train_col, boneage_train_row, model_male_reg_path, model_female_reg_path, model_female_class_path, model_male_class_path
from data_boneage import load_validation_full, load_train_full, load_train_male, load_train_female, load_validation_female, load_validation_male
from keras.preprocessing.image import ImageDataGenerator
from keras.applications.vgg16 import VGG16

K.set_image_data_format('channels_last')  # TF dimension ordering in this code

train_full = True
train_male = True
train_female = True
use_datagen = True

batch_size = 64
epochs = 300
load_weights = False
input_shape = (boneage_train_row, boneage_train_col, 3)

def preprocess(imgs):
	print(imgs.shape)
	for i in range(imgs.shape[0]):
		imgs[i] = resize(imgs[i], (boneage_train_row, boneage_train_col), preserve_range=True)

	imgs = np.stack((imgs,)*3, -1)
	return imgs

def get_vgg16():
	model = VGG16(weights='imagenet', include_top=False, input_shape=input_shape)

	for layer in model.layers[:11]:
	    layer.trainable = False

	flatten = Flatten()(model.output)
	drop1 = Dropout(0.5)(flatten)
	dense1 = Dense(8)(drop1)
	relu1 = Activation('relu')(dense1)
	dense3 = Dense(1)(relu1)

	model = Model(inputs=model.input, outputs=dense3)
	model.compile(optimizer=Adam(lr=1e-4), 	
		loss='mean_absolute_error')
	return model

def get_boneage_model():
	return get_vgg16()

def get_male_model():
	model = VGG16(weights='imagenet', include_top=False, input_shape=input_shape)

	for layer in model.layers[:11]:
	    layer.trainable = False

	flatten = Flatten()(model.output)
	drop1 = Dropout(0.5)(flatten)
	dense1 = Dense(8)(drop1)
	relu1 = Activation('relu')(dense1)
	dense3 = Dense(1)(relu1)

	model = Model(inputs=model.input, outputs=dense3)
	model.compile(optimizer=Adam(lr=1e-4), 	
		loss='mean_absolute_error')
	return model

def get_female_model():
	model = VGG16(weights='imagenet', include_top=False, input_shape=input_shape)

	for layer in model.layers[:11]:
	    layer.trainable = False

	flatten = Flatten()(model.output)
	drop1 = Dropout(0.5)(flatten)
	dense1 = Dense(8)(drop1)
	relu1 = Activation('relu')(dense1)
	dense3 = Dense(1)(relu1)

	model = Model(inputs=model.input, outputs=dense3)
	model.compile(optimizer=Adam(1e-4), 	
		loss='mean_absolute_error')
	return model

def train(X_train, Y_train, X_val, Y_val, model, model_name, load_weights=False):
	model.summary()
	if load_weights:
		model.load_weights(model_name)

	early_stopping = EarlyStopping(patience=15, verbose=1)
	model_checkpoint = ModelCheckpoint(model_name, monitor='val_loss', save_best_only=True)
	reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.5, patience=5, 
		verbose=1, mode='auto', epsilon=0.0001, cooldown=2, min_lr=1e-6)
	history = None
	if use_datagen:
		train_datagen = ImageDataGenerator(
			width_shift_range=0.1,
			height_shift_range=0.1,
			zoom_range=0.1)

		val_datagen = ImageDataGenerator()

		aug_train = train_datagen.flow(X_train, Y_train, batch_size=batch_size)
		aug_val = val_datagen.flow(X_val, Y_val, batch_size=batch_size)

		history = model.fit_generator(aug_train, 
			steps_per_epoch=len(X_train)/batch_size, 
			epochs=epochs, 
			verbose=1,
			validation_data=aug_val,
			validation_steps=len(X_val)/batch_size,
			callbacks=[model_checkpoint, early_stopping, reduce_lr])
	else:
		history = model.fit(X_train, Y_train, verbose=1, epochs=epochs,
			batch_size=batch_size,
			validation_split=0.1,
			shuffle=True,
			callbacks=[model_checkpoint, early_stopping, reduce_lr])
	return history

def plot_training_loss(history, title):
	print(history.history.keys())
	plt.plot(history.history['loss'])
	plt.plot(history.history['val_loss'])
	plt.title(title)
	plt.ylabel('loss')
	plt.xlabel('epoch')
	plt.legend(['train', 'validation'], loc='upper right')
	plt.savefig(title + '.png', bbox_inches='tight')

if __name__ == '__main__':
	# train full data
	if train_full:
		imgs_full, labels_reg_full = load_train_full()
		imgs_full = preprocess(imgs_full)
		imgs_full_val, labels_reg_full_val = load_validation_full()
		imgs_full_val = preprocess(imgs_full_val)

		if not use_datagen:
			imgs_full = np.concatenate((imgs_full, imgs_full_val))
			labels_reg_full = np.concatenate((labels_reg_full, labels_reg_full_val))

		print('Training regression on full data')
		history_reg_full = train(imgs_full, labels_reg_full, imgs_full_val, 
			labels_reg_full_val, 
			get_boneage_model(), 
			model_full_reg_path, 
			load_weights)

	# train male data
	if train_male:
		imgs_male, labels_reg_male = load_train_male()
		imgs_male = preprocess(imgs_male)
		imgs_male_val, labels_reg_male_val = load_validation_male()
		imgs_male_val = preprocess(imgs_male_val)

		if not use_datagen:
			imgs_male = np.concatenate((imgs_male, imgs_male_val))
			labels_reg_male = np.concatenate((labels_reg_male, labels_reg_male_val))

		print('Training regression on male data')
		history_reg_male = train(imgs_male, labels_reg_male, imgs_male_val, 
			labels_reg_male_val, 
			get_male_model(), 
			model_male_reg_path, 
			load_weights)

	# train female data
	if train_female:
		imgs_female, labels_reg_female = load_train_female()
		imgs_female = preprocess(imgs_female)
		imgs_female_val, labels_reg_female_val = load_validation_female()
		imgs_female_val = preprocess(imgs_female_val)

		if not use_datagen:
			imgs_female = np.concatenate((imgs_female, imgs_female_val))
			labels_reg_female = np.concatenate((labels_reg_female, labels_reg_female_val))
			print(imgs_female.shape)
			print(labels_reg_female.shape)

		print('Training regression on female data')
		history_reg_female = train(imgs_female, labels_reg_female, imgs_female_val, 
			labels_reg_female_val, 
			get_female_model(), 
			model_female_reg_path, 
			load_weights)

	# plot training loss
	if train_full:
		plot_training_loss(history_reg_full, 'full regression')

	if train_male:
		plot_training_loss(history_reg_male, 'male regression')

	if train_female:
		plot_training_loss(history_reg_female, 'female regression')