import os
from pandas import DataFrame, read_csv
import pandas as pd
from skimage.io import imsave, imread, imshow
from skimage.transform import resize
from matplotlib import pyplot as plt
from constants import boneage_test_path_maskonly, boneage_test_path_raw, boneage_val_split, normalized_image_path, boneage_csv_path, boneage_train_col, boneage_train_row, boneage_test_path
from os.path import join
import numpy as np
from keras.utils import to_categorical
from sklearn.model_selection import train_test_split
from data_unet import make_square_image

create_full = False
create_male = True
create_female = True

def preprocess(img):
	img = resize(img, output_shape=(boneage_train_row, 200), preserve_range=False)
	img = make_square_image(img, boneage_train_row)
	return img

def create_train(boneage_csv):
	print('Starting creating training data')
	# load images
	image_names = os.listdir(normalized_image_path)
	image_names.sort()
	total = len(image_names)

	if create_male:
		male_image_names = []

	if create_female:
		female_image_names = []

	if create_full:
		imgs_full = np.ndarray((total, boneage_train_row, boneage_train_col), dtype=np.float32)
		labels_reg_full = np.ndarray((total,), dtype=np.uint16)

	# count gender type
	i = 0
	for image_name in image_names:
		img_id = int(image_name.replace('.png', ''))

		if create_full:
			print(image_name)
			img = imread(join(normalized_image_path, image_name), as_grey=True)
			img = preprocess(img)
			img = np.array([img], dtype=np.float32)
			imgs_full[i] = img
			age = int(boneage_csv.loc[img_id, 'boneage'])
			labels_reg_full[i] = age

		if create_male or create_female:
			is_male = boneage_csv.loc[img_id, 'male']

			if is_male:
				male_image_names.append(image_name)
			else:
				female_image_names.append(image_name)
		i += 1

	if create_full:
		imgs_full, imgs_full_val, labels_reg_full, labels_reg_full_val = train_test_split(
			imgs_full, labels_reg_full, test_size=boneage_val_split, random_state=33)

		# save train data mixed
		np.save('boneage_image_full.npy', imgs_full)
		np.save('boneage_image_full_val.npy', imgs_full_val)
		np.save('boneage_label_reg_full.npy', labels_reg_full)
		np.save('boneage_label_reg_full_val.npy', labels_reg_full_val)


	# male data
	if create_male:
		male_count = len(male_image_names)
		imgs_male = np.ndarray((male_count, boneage_train_row, boneage_train_col), dtype=np.float32)
		labels_reg_male = np.ndarray((male_count,), dtype=np.uint16)
		i = 0
		for image_name in male_image_names:
			print(image_name)
			img = imread(join(normalized_image_path, image_name), as_grey=True)
			img = preprocess(img)

			img = np.array([img], dtype=np.float32)
			imgs_male[i] = img
			img_id = int(image_name.replace('.png', ''))

			# get age
			age = int(boneage_csv.loc[img_id, 'boneage'])
			labels_reg_male[i] = age
			i += 1

		imgs_male, imgs_male_val, labels_reg_male, labels_reg_male_val = train_test_split(
			imgs_male, labels_reg_male, test_size=boneage_val_split, random_state=22)


		# save train data male
		np.save('boneage_image_male.npy', imgs_male)
		np.save('boneage_label_reg_male.npy', labels_reg_male)
		# save validation data male
		np.save('boneage_image_male_val.npy', imgs_male_val)
		np.save('boneage_label_reg_male_val.npy', labels_reg_male_val)

	# female data
	if create_female:
		female_count = len(female_image_names)
		imgs_female = np.ndarray((female_count, boneage_train_row, boneage_train_col), dtype=np.float32)
		labels_reg_female = np.ndarray((female_count,), dtype=np.uint16)
		i = 0
		for image_name in female_image_names:
			print(image_name)
			img = imread(join(normalized_image_path, image_name), as_grey=True)
			img = preprocess(img)

			img = np.array([img], dtype=np.float32)
			imgs_female[i] = img
			img_id = int(image_name.replace('.png', ''))

			# get age
			age = int(boneage_csv.loc[img_id, 'boneage'])
			labels_reg_female[i] = age
			i += 1

		imgs_female, imgs_female_val, labels_reg_female, labels_reg_female_val = train_test_split(
			imgs_female, labels_reg_female, test_size=boneage_val_split, random_state=11)

		# save train data female
		np.save('boneage_image_female.npy', imgs_female)
		np.save('boneage_label_reg_female.npy', labels_reg_female)
		# save validation data female
		np.save('boneage_image_female_val.npy', imgs_female_val)
		np.save('boneage_label_reg_female_val.npy', labels_reg_female_val)

def create_test(boneage_csv):
	print('Starting creating testing data')
	# Full preprocessing data
	image_names = os.listdir(boneage_test_path)
	image_names.sort()
	total = len(image_names)
	imgs_test = np.ndarray((total, boneage_train_row, boneage_train_col), dtype=np.float32)
	imgs_test_id = np.ndarray((total,), dtype=np.int32)
	labels_test = np.ndarray((total,), dtype=np.int32)

	i = 0
	for image_name in image_names:
		print(image_name)
		img_id = int(image_name.split('.')[0])
		img = imread(join(boneage_test_path, image_name), as_grey=True)
		img = preprocess(img)
		img = np.array([img], dtype=np.float32)
		imgs_test[i] = img
		imgs_test_id[i] = img_id
		age = int(boneage_csv.loc[img_id, 'boneage'])
		labels_test[i] = age
		i += 1
	
	np.save('boneage_test_image.npy', imgs_test)
	np.save('boneage_test_id.npy', imgs_test_id)
	np.save('boneage_test_label.npy', labels_test)

	# Mask preprocessing only
	image_names = os.listdir(boneage_test_path_maskonly)
	image_names.sort()
	total = len(image_names)
	imgs_test = np.ndarray((total, boneage_train_row, boneage_train_col), dtype=np.float32)

	i = 0
	for image_name in image_names:
		img = imread(join(boneage_test_path_maskonly, image_name), as_grey=True)
		img = preprocess(img)
		img = np.array([img], dtype=np.float32)
		imgs_test[i] = img
		i += 1
	
	np.save('boneage_test_image_maskonly.npy', imgs_test)

	# Raw
	image_names = os.listdir(boneage_test_path_raw)
	image_names.sort()
	total = len(image_names)
	imgs_test = np.ndarray((total, boneage_train_row, boneage_train_col), dtype=np.float32)

	i = 0
	for image_name in image_names:
		img = imread(join(boneage_test_path_raw, image_name), as_grey=True)
		img = preprocess(img)
		img = np.array([img], dtype=np.float32)
		imgs_test[i] = img
		i += 1
	
	np.save('boneage_test_image_raw.npy', imgs_test)


def load_train_male():
	imgs = np.load('boneage_image_male.npy')
	labels_reg_male = np.load('boneage_label_reg_male.npy')
	return imgs, labels_reg_male

def load_train_female():
	imgs = np.load('boneage_image_female.npy')
	labels_reg_female = np.load('boneage_label_reg_female.npy')
	return imgs, labels_reg_female

def load_test():
	imgs_test = np.load('boneage_test_image.npy')
	imgs_test_id = np.load('boneage_test_id.npy')
	labels_test = np.load('boneage_test_label.npy')
	return imgs_test, imgs_test_id, labels_test

def load_test_maskonly():
	imgs_test = np.load('boneage_test_image_maskonly.npy')
	return imgs_test

def load_test_raw():
	imgs_test = np.load('boneage_test_image_raw.npy')
	return imgs_test

def load_validation_male():
	imgs_val = np.load('boneage_image_male_val.npy')
	labels_reg_male_val = np.load('boneage_label_reg_male_val.npy')
	return imgs_val, labels_reg_male_val

def load_validation_female():
	imgs_val = np.load('boneage_image_female_val.npy')
	labels_reg_female_val = np.load('boneage_label_reg_female_val.npy')
	return imgs_val, labels_reg_female_val

def load_train_full():
	imgs = np.load('boneage_image_full.npy')
	labels_reg = np.load('boneage_label_reg_full.npy')
	return imgs, labels_reg

def load_validation_full():
	imgs_val = np.load('boneage_image_full_val.npy')
	labels_reg_full_val = np.load('boneage_label_reg_full_val.npy')
	return imgs_val, labels_reg_full_val

if __name__ == '__main__':
	# load labels
	boneage_csv = pd.read_csv(boneage_csv_path)
	boneage_csv.set_index('id', inplace=True)

	create_train(boneage_csv)
	create_test(boneage_csv)