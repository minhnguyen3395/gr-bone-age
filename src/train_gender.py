from keras.models import Model, Sequential
from keras.layers import Input, Conv2D, Dense, Dropout, MaxPooling2D, Activation, BatchNormalization, Flatten
from keras.callbacks import ModelCheckpoint, EarlyStopping, ReduceLROnPlateau
import numpy as np
from keras import backend as K
from skimage.io import imshow
from skimage.transform import resize, rotate
import matplotlib.pyplot as plt
from keras.optimizers import Adam, RMSprop
from constants import boneage_train_col, boneage_train_row, model_gender_path
from data_gender import load_train, load_validation
from keras.preprocessing.image import ImageDataGenerator
from keras.applications.vgg16 import VGG16
from train_boneage import preprocess

K.set_image_data_format('channels_last')  # TF dimension ordering in this code

batch_size = 64
epochs = 300
load_weights = False
input_shape = (boneage_train_row, boneage_train_col, 3)

def get_gender_model():
	return get_vgg16()

def get_vgg16():
	model = VGG16(weights = "imagenet", include_top=False, input_shape=input_shape)

	for layer in model.layers[:11]:
	    layer.trainable = False

	flatten = Flatten()(model.output)
	drop1 = Dropout(0.5)(flatten)
	dense1 = Dense(64)(drop1)
	relu1 = Activation('relu')(dense1)
	dense3 = Dense(2)(relu1)
	softmax = Activation('softmax')(dense3)

	model = Model(inputs=model.input, outputs=softmax)
	model.compile(optimizer=Adam(lr=1e-5), 
		loss='categorical_crossentropy', 
		metrics=['accuracy'])
	return model

def train(X_train, Y_train, X_val, Y_val, model, model_name, load_weights=False):
	model.summary()
	if load_weights:
		model.load_weights(model_name)

	early_stopping = EarlyStopping(patience=10, verbose=1)
	model_checkpoint = ModelCheckpoint(model_name, monitor='val_loss', save_best_only=True)
	reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.5, patience=3, 
		verbose=1, mode='auto', epsilon=0.0001, cooldown=1, min_lr=1e-6)

	train_datagen = ImageDataGenerator(
		width_shift_range=0.1,
		height_shift_range=0.1,
		zoom_range=0.1)

	val_datagen = ImageDataGenerator()

	aug_train = train_datagen.flow(X_train, Y_train, batch_size=batch_size)
	aug_val = val_datagen.flow(X_val, Y_val, batch_size=batch_size)

	history = model.fit_generator(aug_train, 
		steps_per_epoch=len(X_train)/batch_size, 
		validation_data=aug_val, 
		validation_steps=len(X_val)/batch_size, 
		epochs=epochs, 
		verbose=1,
		callbacks=[model_checkpoint, early_stopping, reduce_lr])

	return history

if __name__ == '__main__':
	imgs, labels_gender = load_train()
	imgs = preprocess(imgs)
	imgs_val, labels_gender_val = load_validation()
	imgs_val = preprocess(imgs_val)

	history = train(imgs, labels_gender, imgs_val, labels_gender_val, 
		get_vgg16(), model_gender_path, load_weights)

	print(history.history.keys())

	plt.plot(history.history['acc'])
	plt.plot(history.history['val_acc'])
	plt.title('model accuracy')
	plt.ylabel('accuracy')
	plt.xlabel('epoch')
	plt.legend(['train', 'val'], loc='upper left')
	# plt.savefig('gender_acc.png', bbox_inches='tight')
	plt.show()

	plt.plot(history.history['loss'])
	plt.plot(history.history['val_loss'])
	plt.title('model loss')
	plt.ylabel('loss')
	plt.xlabel('epoch')
	plt.legend(['train', 'val'], loc='upper left')
	plt.show()
	# plt.savefig('gender_loss.png', bbox_inches='tight')