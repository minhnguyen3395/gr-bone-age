import os
import shutil
from constants import boneage_test_path, unet_train_path, unet_mask_path

image_names = os.listdir(boneage_test_path)

for image_name in image_names:
	shutil.move(os.path.join(unet_train_path, image_name), os.path.join('../legacy/train', image_name))
	shutil.move(os.path.join(unet_mask_path, image_name), os.path.join('../legacy/mask', image_name))