# gr-bone-age
###Environment
-------------
- Centos 7
- Python 2.7.5
- virtualenv 15.2.0 (optional)

###Package dependencies
-------------
- pip
- tensorflow gpu 1.4.0
- Keras 2.1.5
- cv2 3.4.0 (OpenCV for python)
- skimage 0.13.1
- matplotlib 2.2.2
- numpy 1.14.2
- Tkinter (Revision: 81008)
- PIL 5.1.0
- pandas 0.22.0
- h5py 2.7.1
- scipy 1.0.1
- sklearn 0.19.1
- CUDA V8.0.61
- cudnn 6

###Installing dependencies
-------------
- Pip
	`yum install python-pip`
- numpy
	`pip install numpy`
- h5py
	`pip install h5py`
- pandas
	`pip install pandas`
- cv2
	`yum install opencv*`
- tensorflow
	`pip install tensorflow==1.4.0`
- scipy
	`pip install scipy`
- matplotlib
	`pip install matplotlib`
- skimage
	`pip install scikit-image`
- sklearn
	`pip install scikit-learn`
- PIL
	`pip install pillow`

###Project structure
-------------
- gr
	- src/ (contain source code and data npy files)
	- normalized/ (contain all normalized images)
	- boneage-test/
	- boneage-training-dataset/ (contain full raw dataset)
	- keypoints/
	  - manual/
	  - test/
	  - train/
	  - key_points.json
	  - kp_full.json
	- mask/
	  - mask-ann/
	  - mask-final/
	  - mask-good-raw/
	  - mask-raw/
	  - unet-mask/
	    - train/
	  - unet-test/
	  - unet-train/
	    - train/

###How to run
-------------
- This is [link](https://drive.google.com/open?id=1t29cdHDWyhRjSi8gnEzqrYA8UVQWJdze) to the data and weight folder. You just download the folder and move its content into `src` folder.

- We already run the data preparation code and saved into npy files, so no more data preprocessing is required.
- We also save the best weight of all models so you basically only need to run test experiments.

- To test gender model run `python test_gender.py`.
- To test the boneage assessment run `python test_boneage.py` The field `use_gender_in_csv` in source file determines if gender classification model should be used or not.

- If you want to train from scratch, you can follow the below instruction:
  - Run `python train_gender.py` to train gender model.
  - Run `python train_boneage.py` to train boneage model. You can change training option in source file like selective dataset (full, male, female), load_weights if you want to retrain from previous weight.